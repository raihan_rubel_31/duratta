class ChangeStatusOnOrders < ActiveRecord::Migration
  def change
    change_column :orders, :status, :string, default: Order::STATUS[:pending]
  end
end
