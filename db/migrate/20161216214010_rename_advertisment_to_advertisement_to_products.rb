class RenameAdvertismentToAdvertisementToProducts < ActiveRecord::Migration
  def change
    rename_column :products, :advertisment, :advertisement
  end
end
