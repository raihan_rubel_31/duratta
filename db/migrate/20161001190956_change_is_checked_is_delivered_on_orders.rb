class ChangeIsCheckedIsDeliveredOnOrders < ActiveRecord::Migration
  def change
    change_column :orders, :is_delivered, :boolean, default: false
    change_column :orders, :is_checked, :boolean, default: false
  end
end
