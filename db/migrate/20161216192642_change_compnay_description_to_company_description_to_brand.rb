class ChangeCompnayDescriptionToCompanyDescriptionToBrand < ActiveRecord::Migration
  def change
    rename_column :brands, :compnay_description, :company_description
  end
end
