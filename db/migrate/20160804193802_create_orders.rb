class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.boolean :is_delivered
      t.boolean :is_checked
      t.string :status
      t.integer :items
      t.decimal :total_price

      t.timestamps null: false
    end
  end
end
