class AddPurchasedQuantityToProducts < ActiveRecord::Migration
  def change
    add_column :products, :purchased_quantity, :integer
  end
end
