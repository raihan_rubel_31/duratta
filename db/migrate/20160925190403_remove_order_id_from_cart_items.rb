class RemoveOrderIdFromCartItems < ActiveRecord::Migration
  def change
    remove_column :cart_items, :order_id
  end
end
