class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.string :company_name
      t.text :compnay_description
      t.text :brand_description

      t.timestamps null: false
    end
  end
end
