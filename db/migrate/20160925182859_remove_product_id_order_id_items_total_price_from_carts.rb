class RemoveProductIdOrderIdItemsTotalPriceFromCarts < ActiveRecord::Migration
  def change
    remove_column :carts, :product_id
    remove_column :carts, :order_id
    remove_column :carts, :items
    remove_column :carts, :total_price
  end
end
