class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.integer :product_id
      t.integer :order_id
      t.integer :items
      t.decimal :total_price

      t.timestamps null: false
    end
  end
end
