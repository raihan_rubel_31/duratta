class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.integer :product_id
      t.integer :order_id
      t.decimal :quantity

      t.timestamps null: false
    end
  end
end
