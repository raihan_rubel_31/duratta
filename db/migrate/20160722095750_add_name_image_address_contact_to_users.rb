class AddNameImageAddressContactToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :address, :text
    add_column :users, :image, :string
    add_column :users, :contact, :string
  end
end
