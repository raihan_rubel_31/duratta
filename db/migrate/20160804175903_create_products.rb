class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price
      t.decimal :old_price
      t.string :condition
      t.boolean :is_available
      t.text :description
      t.integer :brand_id
      t.string :u_id
      t.decimal :rating
      t.integer :rank_voted

      t.timestamps null: false
    end
  end
end
