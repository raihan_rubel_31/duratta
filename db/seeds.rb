# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Category.create([{name: 'Sportswear'},{name:'Mens'},{name:'Womens'},{name:'Kids'},{name:'Fashion'},{name:'Households'},{name:'Interiors'},{name:'Clothing'},{name:'Bags'},{name:'Shoes'}])
# Category.create([{name: 'Nike', category_id: 1},{name:'Under Armour', category_id: 1},{name:'Adidas ', category_id: 1},{name:'Puma', category_id: 1},{name:'ASICS', category_id: 1},{name:'Fendi', category_id: 2},{name:'Guess', category_id: 2},{name:'Valentino',category_id: 2 },{name:'Dior',category_id: 2},{name:'Versace',category_id: 2},{name:'Armani',category_id: 2},{name:'Prada',category_id: 2},{name:'Dolce and Gabbana',category_id: 2},{name:'Chanel',category_id: 2},{name:'Gucci',category_id: 2},{name:'Fendi',category_id: 3},{name:'Guess',category_id: 3},{name:'Valentino',category_id: 3},{name:'Dior',category_id: 3},{name:'Versace',category_id: 3}])
# Brand.create([{name: 'Acne', company_name: 'creative culture'},{name: 'Grüne Erde', company_name: 'creative culture'},{name: 'Albiro', company_name: 'creative culture'},{name: 'Ronhill', company_name: 'creative culture'},{name: 'Oddmolly', company_name: 'creative culture'},{name: 'Boudestijn', company_name: 'creative culture'},{name: 'creative culture', company_name: 'creative culture'}])
# Product.create([
#                    {name: 'Easy Polo Black Edition', image: 'home/product1.jpg', price: 56, rating: 4},
#                    {name: 'Easy Polo Black Edition', image: 'home/product2.jpg', price: 56, rating: 4},
#                    {name: 'Easy Polo Black Edition', image: 'home/product3.jpg', price: 56, rating: 4},
#                    {name: 'Easy Polo Black Edition', image: 'home/product4.jpg', price: 56, rating: 4},
#                    {name: 'Easy Polo Black Edition', image: 'home/product5.jpg', price: 56, rating: 4},
#                    {name: 'Easy Polo Black Edition', image: 'home/product6.jpg', price: 56, rating: 4},
#                ])
# Category.where(category_id: nil).each do |category|
#   category.sub_categories.each do |sub_category|
#       10.times.each do
#         sub_category.sub_products.create(name: 'Easy Polo Black Edition', image: 'home/product1.jpg', price: 56, rating: 4)
#       end
#     end
# end
#
# Brand.all.each do |brand|
#   6.times.each do
#     brand.products.create(name: 'Easy Polo Black Edition', image: 'home/product1.jpg', price: 56, rating: 4)
#   end
# end
AdminUser.create!(email: 'raihan@admin.com', password: 'password', password_confirmation: 'password')
