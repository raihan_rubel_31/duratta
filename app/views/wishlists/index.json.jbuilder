json.array!(@wishlists) do |wishlist|
  json.extract! wishlist, :id, :product_id, :user_id, :priority
  json.url wishlist_url(wishlist, format: :json)
end
