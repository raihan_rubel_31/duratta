json.array!(@reviews) do |review|
  json.extract! review, :id, :comment, :user_id, :product_id, :rated
  json.url review_url(review, format: :json)
end
