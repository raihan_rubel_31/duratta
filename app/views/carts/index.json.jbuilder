json.array!(@carts) do |cart|
  json.extract! cart, :id, :product_id, :order_id, :items, :total_price
  json.url cart_url(cart, format: :json)
end
