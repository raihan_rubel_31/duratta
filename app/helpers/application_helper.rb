module ApplicationHelper
  def global_categories
    Category.where(category_id: nil).includes(:sub_categories)
  end

  def global_brands
    Brand.all
  end

  def current_cart_items
    cart_item = 0
    if session[:cart_id].present?
      cart_item = CartItem.where(cart_id: session[:cart_id]).count
    end
    cart_item
  end

  def add_to_cart_text(product)
    cart_item_by_product = current_cart.cart_items.where(product_id: product.id).first
    html = ''
    if cart_item_by_product.present? && cart_item_by_product.quantity > 0
      html +=  link_to "<i class='fa fa-plus'></i>".html_safe, cart_path(id: cart_item_by_product.id, incr: true), method: :put, remote: true, class: "btn btn-secondary verticle-border"
      html +=  link_to cart_item_by_product.quantity, carts_path(product: product.to_param), method: :post, remote: true, class: "btn btn-secondary verticle-border"
      html +=  link_to "<i class='fa fa-minus'></i>".html_safe, cart_path(id: cart_item_by_product.id, incr: false), method: :put, remote: true, class: "btn btn-secondary"
    else

      html +=  link_to "<i class='fa fa-shopping-cart'></i>".html_safe, carts_path(product: product.to_param), method: :post, remote: true, class: 'btn btn-secondary verticle-border'
      html +=  link_to "Add to cart", carts_path(product: product.to_param), method: :post, remote: true, class: "btn btn-secondary"
    end

   html
  end

  def activation_checker(path)
    action_hash = Rails.application.routes.recognize_path(path)
    if params[:category].present?
      'active' if params[:controller] == action_hash[:controller] && params[:action] == action_hash[:action] && params[:category] == action_hash[:category]
    elsif params[:brand].present?
      'active' if params[:controller] == action_hash[:controller] && params[:action] == action_hash[:action] && params[:brand] == action_hash[:brand]
    else
      'active' if params[:controller] == action_hash[:controller] && params[:action] == action_hash[:action]
    end
  end

  def place_order_link
    if user_signed_in?
        link_to 'Place Order', orders_path(cart: current_cart), method: :post, class: 'btn btn-default cart'
    else
        link_to 'Place Order', new_user_session_path, remote: true, class: 'btn btn-default cart'
    end
  end



end
