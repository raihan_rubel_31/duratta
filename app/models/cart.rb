class Cart < ActiveRecord::Base
  belongs_to :user
  has_many :cart_items, dependent: :destroy

  def add_product(product_id, item)
    cart_item = cart_items.find_by_product_id(product_id)
    if cart_item
      cart_item.quantity = cart_item.quantity.to_i + item.to_i
    else
      cart_item = cart_items.build(product_id: product_id, quantity: item)
    end
    cart_item
  end

  def total_cart_price
    self.cart_items.collect { |ci| ci.quantity * ci.product.price }.sum
  end

end