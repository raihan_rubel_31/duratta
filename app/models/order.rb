class Order < ActiveRecord::Base
  belongs_to :user
  has_many :stores
  STATUS = {
      pending: 'Pending',
      checked: 'Checked',
      onway: 'Onway',
      delivered: 'Delivered'
  }
end
