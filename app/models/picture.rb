class Picture < ActiveRecord::Base
  mount_uploader :url
  belongs_to :product
end
