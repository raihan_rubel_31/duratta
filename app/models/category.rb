class Category < ActiveRecord::Base
  extend FriendlyId

  mount_uploader :image, ImageUploader
  has_many :products
  has_many :sub_categories, class_name: 'Category', foreign_key: :category_id
  belongs_to :category, class_name: 'Category', foreign_key: :category_id
  has_many :sub_products, class_name: 'Product', foreign_key: :sub_category_id
  validates_uniqueness_of :name

  friendly_id :name, use: [:slugged, :finders]


end
