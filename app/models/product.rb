class Product < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  belongs_to :category
  belongs_to :sub_category, class_name: 'Category', foreign_key: :sub_category_id
  belongs_to :brand
  has_many :cart_items
  has_many :pictures
  has_many :reviews
  has_many :stores
  has_many :wishlists
  before_destroy :ensure_product_not_in_cart_item

  private
  def ensure_product_not_in_cart_item
    if cart_items.present?
      errors.add(:base, 'Cart items present')
      return false
    else
      return true
    end
  end
end
