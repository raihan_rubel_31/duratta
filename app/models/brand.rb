class Brand < ActiveRecord::Base
  mount_uploader :image
  has_many :products
end
