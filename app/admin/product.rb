ActiveAdmin.register Product do
  permit_params :name, :price, :old_price, :condition, :is_available, :description, :brand_id, :u_id, :rating, :rank_voted, :category_id, :sub_category_id, :advertisement, :image, :purchased_quantity


  index as: :grid do |product|
    link_to image_tag(product.image_url(:thumb)), admin_product_path(product)
  end

  filter :u_id
  filter :name
  filter :price
  filter :rating
  filter :created_at

  form do |f|
    f.inputs "Product Details" do
      f.input :category
      f.input :sub_category
      f.input :brand
      f.input :u_id
      f.input :name
      f.input :price
      f.input :old_price
      f.input :condition
      f.input :description
      f.input :is_available
      f.input :advertisement
      f.input :image
    end
    f.actions
  end
end
