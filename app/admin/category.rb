ActiveAdmin.register Category do
  permit_params :name, :image, :category_id
  index do
    selectable_column
    id_column
    column :name
    column :category
    column :image
    actions
  end

  filter :id
  filter :name

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs " Details" do
      f.input :name
      f.input :category
      f.input :image
    end
    f.actions
  end
end
