ActiveAdmin.register Brand do

  permit_params :name, :company_name, :company_description, :brand_description

  index do
    selectable_column
    id_column
    column :name
    column :company_name
    column :company_description
    column :brand_description
    actions
  end

  filter :id
  filter :company_name

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Brand Details" do
      f.input :name
      f.input :company_name
      f.input :company_description
      f.input :brand_description
    end
    f.actions
  end


end
