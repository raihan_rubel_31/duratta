//var LoginModalController = {
//    tabsElementName: ".logmod__tabs li",
//    tabElementName: ".logmod__tab",
//    inputElementsName: ".logmod__form .input",
//    hidePasswordName: ".hide-password",
//
//    inputElements: null,
//    tabsElement: null,
//    tabElement: null,
//    hidePassword: null,
//
//    activeTab: null,
//    tabSelection: 0, // 0 - first, 1 - second
//
//    findElements: function () {
//        var base = this;
//
//        base.tabsElement = $(base.tabsElementName);
//        base.tabElement = $(base.tabElementName);
//        base.inputElements = $(base.inputElementsName);
//        base.hidePassword = $(base.hidePasswordName);
//
//        return base;
//    },
//
//    setState: function (state) {
//    	var base = this,
//            elem = null;
//
//        if (!state) {
//            state = 0;
//        }
//
//        if (base.tabsElement) {
//        	elem = $(base.tabsElement[state]);
//            elem.addClass("current");
//            $("." + elem.attr("data-tabtar")).addClass("show");
//        }
//
//        return base;
//    },
//
//    getActiveTab: function () {
//        var base = this;
//
//        base.tabsElement.each(function (i, el) {
//           if ($(el).hasClass("current")) {
//               base.activeTab = $(el);
//           }
//        });
//
//        return base;
//    },
//
//    addClickEvents: function () {
//    	var base = this;
//
//        base.hidePassword.on("click", function (e) {
//            var $this = $(this),
//                $pwInput = $this.prev("input");
//
//            if ($pwInput.attr("type") == "password") {
//                $pwInput.attr("type", "text");
//                $this.text("Hide");
//            } else {
//                $pwInput.attr("type", "password");
//                $this.text("Show");
//            }
//        });
//
//        base.tabsElement.on("click", function (e) {
//            var targetTab = $(this).attr("data-tabtar");
//
//            e.preventDefault();
//            base.activeTab.removeClass("current");
//            base.activeTab = $(this);
//            base.activeTab.addClass("current");
//
//            base.tabElement.each(function (i, el) {
//                el = $(el);
//                el.removeClass("show");
//                if (el.hasClass(targetTab)) {
//                    el.addClass("show");
//                }
//            });
//        });
//
//        base.inputElements.find("label").on("click", function (e) {
//           var $this = $(this),
//               $input = $this.next("input");
//
//            $input.focus();
//        });
//
//        return base;
//    },
//
//    initialize: function () {
//        var base = this;
//
//        base.findElements().setState().getActiveTab().addClickEvents();
//    }
//};
//
//$(document).ready(function() {
//    LoginModalController.initialize();
//});




/*
 *
 * login-register modal
 * Autor: Creative Tim
 * Web-autor: creative.tim
 * Web script: http://creative-tim.com
 *
 */
function showRegisterForm(){
    $('.loginBox').fadeOut('fast',function(){
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast',function(){
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
    });
    $('.error').removeClass('alert alert-danger').html('');

}
function showLoginForm(){
    $('#loginModal .registerBox').fadeOut('fast',function(){
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast',function(){
            $('.login-footer').fadeIn('fast');
        });

        $('.modal-title').html('Login with');
    });
    $('.error').removeClass('alert alert-danger').html('');
}

function openLoginModal(){
    showLoginForm();
    setTimeout(function(){
        $('#loginModal').modal('show');
    }, 230);

}

function openRegisterModal(){
    showRegisterForm();
    setTimeout(function(){
        $('#loginModal').modal('show');
    }, 230);

}

function loginAjax(){
    /*   Remove this comments when moving to server
     $.post( "/login", function( data ) {
     if(data == 1){
     window.location.replace("/home");
     } else {
     shakeModal();
     }
     });
     */

    /*   Simulate error message from the server   */
    shakeModal();
}

function shakeModal(){
    $('#loginModal .modal-dialog').addClass('shake');
    setTimeout( function(){
        $('#loginModal .modal-dialog').removeClass('shake');
    }, 1000 );
}

