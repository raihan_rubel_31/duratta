// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require turbolinks
//= require jquery_ujs
//= require nprogress
//= require nprogress-ajax
//= require nprogress-turbolinks
//= require_tree .


NProgress.configure({
    minimum: 0.08,
    easing: 'ease',
    positionUsing: '',
    speed: 200,
    trickle: true,
    trickleRate: 0.02,
    trickleSpeed: 800,
    showSpinner: true,
    barSelector: '[role="bar"]',
    spinnerSelector: '[role="spinner"]',
    parent: 'body',
    template: '' +
    '<div class="bar" role="bar">' +
    '   <div class="peg"></div>' +
    '</div>' +
    '<div class="spinner-custome" role="spinner">' +
    '   <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>' +
    '</div>'
});
