class HomeController < ApplicationController
  def index
    @categories = Category.where(category_id: nil).includes(:sub_categories)
    @brands = Brand.all
    @products = Product.all.paginate(:page => params[:page], :per_page => 10) #where(rating: 4).paginate(:page => params[:page], :per_page => 10)#where(:all, rating: 4, limit: 6)
    @nav_categories = Category.where(category_id: present?).limit(5)
  end
end
