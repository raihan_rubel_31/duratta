class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    if params[:sub_category].present?
      sub_category = Category.where("lower(name) = ?", params[:sub_category].downcase).first
      @products = sub_category.sub_products.paginate(:page => params[:page], :per_page => 10)
    end

    if params[:brand].present?
      brand = Brand.where('lower(name)= ?', params[:brand].downcase).first
      @products = brand.products.paginate(:page => params[:page], :per_page => 10)
    end

  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  def add_to_wishlist

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :price, :old_price, :condition, :is_available, :description, :brand_id, :u_id, :rating, :rank_voted)
    end
end
