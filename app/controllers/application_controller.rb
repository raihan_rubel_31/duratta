class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_cart, :total_price, :resource_name, :resource, :devise_mapping, :errors_to_message_string
  add_flash_types :success, :info, :warn ,:error
  before_filter :store_current_location, :unless => :devise_controller?

  def current_cart
    Cart.find(session[:cart_id])
  rescue ActiveRecord::RecordNotFound
    cart = Cart.create
    session[:cart_id] = cart.id
    cart
  end

  def total_price
    current_cart.total_cart_price
  end


  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def errors_to_message_string(errors)
    message_wrapper = "<ul class='devise-error-message'>"
    message = ''
    errors.each_with_index do |(key, error_message), index|
      field = (key.to_s == 'custom' || key.to_s == 'password_confirmation') ? '' : key.to_s
      message += "<li> #{field.camelize} #{error_message} </li>"
    end
    message_wrapper + message + '</ul>'
  end

  private

  def store_current_location
    store_location_for(:user, request.url)
  end


end
