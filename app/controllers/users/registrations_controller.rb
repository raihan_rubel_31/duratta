class Users::RegistrationsController < Devise::RegistrationsController
before_action :configure_sign_up_params, only: [:create]
before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    build_resource({})
    set_minimum_password_length
    yield resource if block_given?
    respond_to do |format|
      format.js
    end
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    if resource.save
      if resource.active_for_authentication?
        sign_up(resource_name, resource)
        return render :json => {success: true, redirect_path: after_sign_up_path_for(resource)}
      else
        flash[:info] = t('devise.registrations.signed_up_but_unconfirmed')
        return render :json => {success: true, confirmation_sent: true, redirect_path: after_sign_up_path_for(resource)}
      end
    else
      clean_up_passwords resource
      render :json => {:success => false, message: errors_to_message_string(resource.errors)}
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:first_name, :last_name, :contact, :email, :password, :password_confirmation)
    end
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:first_name, :last_name, :contact, :address, :current_password, :email, :password, :password_confirmation)
    end
  end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)
  end
end
