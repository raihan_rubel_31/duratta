class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :edit, :update, :destroy]
  # GET /carts
  # GET /carts.json
  def index
    @carts = current_cart.cart_items
  end


  # POST /carts
  # POST /carts.json
  def create
    cart = current_cart
    item = 1
    if params[:item].present?
      item = params[:item]
    end
    @cart_item = cart.add_product(params[:product], item)
    respond_to do |format|
      if @cart_item.save
        format.html { redirect_to :back, notice: 'Cart was successfully created.' }
        format.json { render :show, status: :created, location: @cart }
        format.js
      else
        format.html { render :new }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    if params[:incr] == 'true'
      @cart_item.quantity += 1
    else
      if @cart_item.quantity >= 1
        @cart_item.quantity -= 1
      end
    end
    respond_to do |format|
      if @cart_item.save
        format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart_item.destroy
    respond_to do |format|
      format.html { redirect_to carts_url, notice: 'Cart was successfully destroyed.' }
      format.json { head :no_content }
      format.js
    end
  end

  #remove cart from Cart and CartItem
  def remove
    @cart = Cart.find(params[:id])
    @cart_items = CartItem.where(cart_id: @cart.id)
    @cart_items.each do |cart|
      cart.destroy
    end
    session.delete(:@cart)
    @cart.destroy
    respond_to do |format|
      format.js
      format.html
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart_item = CartItem.find(params[:id])
    end

  def set_product(product)
    Product.find_by_id(product)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.require(:cart).permit!
    end
end
