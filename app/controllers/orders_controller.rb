class OrdersController < ApplicationController
  before_filter :authenticate_user!, only: [:create]
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    cart = get_cart(params[:cart])
    @order = current_user.orders.build(items: cart.cart_items.count )
    o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
    @order.serial_no = (0...10).map { o[rand(o.length)] }.join
    @order.total_price = total_price
    if @order.save
      cart.cart_items.each do |cart_item|
        store = @order.stores.build(product_id: cart_item.product_id, quantity: cart_item.quantity)
        if store.save
          cart_item.destroy
        end
      end
      cart.destroy
    end

    respond_to do |format|
      unless @order.errors.present?
        format.html { redirect_to root_path, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :back }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    def get_cart(cart)
      Cart.find(cart)
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:user_id, :is_delivered, :is_checked, :status, :items, :total_price)
    end
end
