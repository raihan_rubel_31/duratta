Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers:{ sessions: "users/sessions", registrations: 'users/registrations', :omniauth_callbacks => "users/omniauth_callbacks"}



  resources :wishlists, only:[:index, :create, :update] do
    get :clear, on: :collection
  end

  resources :carts do
    get :remove, on: :member
  end

  resources :orders, only: [:index, :show, :destroy, :create, :new]


  resources :products, only:[:show] do

  end

  get '/:category/:sub_category'=>'products#index', as: :category_products
  get '/:brand'=>'products#index', as: :brand_products



  resources :posts






  resources :reviews



  root 'home#index'
end
