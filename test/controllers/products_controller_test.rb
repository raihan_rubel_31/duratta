require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { brand_id: @product.brand_id, condition: @product.condition, description: @product.description, is_available: @product.is_available, name: @product.name, old_price: @product.old_price, price: @product.price, rank_voted: @product.rank_voted, rating: @product.rating, u_id: @product.u_id }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: { brand_id: @product.brand_id, condition: @product.condition, description: @product.description, is_available: @product.is_available, name: @product.name, old_price: @product.old_price, price: @product.price, rank_voted: @product.rank_voted, rating: @product.rating, u_id: @product.u_id }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
